package ru.pudovikov.langdetector.Fragments


import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.jakewharton.rxbinding2.view.clicks
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_new_text.view.*
import org.json.JSONObject
import ru.pudovikov.langdetector.AuxiliaryClasses.DialogController
import ru.pudovikov.langdetector.AuxiliaryClasses.HttpRequestController
import ru.pudovikov.langdetector.ViewModels.NewTextFragmentViewModel
import ru.pudovikov.langdetector.R
import kotlinx.android.synthetic.main.fragment_new_text.*
import ru.pudovikov.langdetector.AuxiliaryClasses.ConvertingController
import ru.pudovikov.langdetector.AuxiliaryClasses.KeyboardController
import ru.pudovikov.langdetector.Credentials.WatsonCredentials


class NewTextFragment : Fragment() {

    val compositeDisposable = CompositeDisposable()
    lateinit var dialogController: DialogController
    lateinit var newTextFragmentViewModel: NewTextFragmentViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dialogController = DialogController(context)
        newTextFragmentViewModel = ViewModelProviders.of(this)[NewTextFragmentViewModel::class.java]
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val inflatedView = inflater.inflate(R.layout.fragment_new_text, container, false)

        compositeDisposable.add(inflatedView.detectLanguageButton.clicks().subscribe {
            KeyboardController.hideKeyboard(context!!, view?.windowToken)
            tryToIdentifyLanguage(editText.text.toString())
        })

        return inflatedView
    }

    fun tryToIdentifyLanguage(typedText: String) {
        if (!typedText.isNullOrEmpty()) {
            compositeDisposable.add(
                    newTextFragmentViewModel.detectLanguage(typedText).subscribe({ language ->
                        dialogController
                                .showAlertDialog(getString(R.string.message),
                                        String.format(getString(R.string.detectedLanguage), language))
                                .subscribe().dispose()
                    }, { error ->
                        dialogController
                                .showAlertDialog(getString(R.string.message), error.message!!)
                                .subscribe().dispose()
                    })
            )
        }
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        super.onDestroy()
    }
}
