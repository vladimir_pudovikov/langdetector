package ru.pudovikov.langdetector.Fragments


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_history.view.*
import ru.pudovikov.langdetector.Adapters.HistoryRecyclerViewAdapter
import ru.pudovikov.langdetector.AuxiliaryClasses.DialogController
import ru.pudovikov.langdetector.Models.HistoryModel
import ru.pudovikov.langdetector.R
import ru.pudovikov.langdetector.ViewModels.HistoryViewModel
import ru.pudovikov.langdetector.ViewModels.NewTextFragmentViewModel
import androidx.recyclerview.widget.DividerItemDecoration


class HistoryFragment : Fragment() {

    val compositeDisposable = CompositeDisposable()
    lateinit var historyRecyclerViewAdapter: HistoryRecyclerViewAdapter
    lateinit var historyViewModel: HistoryViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        historyViewModel = ViewModelProviders.of(this)[HistoryViewModel::class.java]
        historyRecyclerViewAdapter = HistoryRecyclerViewAdapter(context!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val inflatedView = inflater.inflate(R.layout.fragment_history, container, false)

        with(inflatedView.historyRecyclerView) {
            layoutManager = LinearLayoutManager(this.context, RecyclerView.VERTICAL, false)
            addItemDecoration(DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL))
            adapter = historyRecyclerViewAdapter
        }

        compositeDisposable.add(
            historyViewModel.getAllHistory()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{ arrayListOfHistoryModels ->
                historyRecyclerViewAdapter.update(arrayListOfHistoryModels)
            })
        // inflate the layout for this fragment
        return inflatedView
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        super.onDestroy()
    }
}
