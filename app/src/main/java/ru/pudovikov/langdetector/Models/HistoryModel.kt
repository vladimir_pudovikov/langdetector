package ru.pudovikov.langdetector.Models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import java.util.*

open class HistoryModel(var sourceText: String? = null, var language: String? = null): RealmObject()