package ru.pudovikov.langdetector.Constants

class HttpErrorConstants {
    companion object {
        const val FAILED = "Запрос не выполнен"
        const val NO_SUCCESS = "Запрос завершился с ошибкой"
    }
}