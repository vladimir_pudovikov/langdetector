package ru.pudovikov.langdetector.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.pudovikov.langdetector.Models.HistoryModel
import ru.pudovikov.langdetector.R
import kotlinx.android.synthetic.main.recycler_view_list_item.view.*

class HistoryRecyclerViewAdapter(val context: Context): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val historyModels = ArrayList<HistoryModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return HistoryViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.recycler_view_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return historyModels.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val holder = holder as HistoryViewHolder
        holder.bind(historyModels.get(position))
    }

    fun update(historyModels: ArrayList<HistoryModel>) {
        this.historyModels.clear()
        this.historyModels.addAll(historyModels)
        notifyDataSetChanged()
    }

    private class HistoryViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        fun bind(historyModel: HistoryModel) = with(itemView) {
            sourceTextView.text = historyModel.sourceText
            languageTextView.text = historyModel.language
        }
    }
}