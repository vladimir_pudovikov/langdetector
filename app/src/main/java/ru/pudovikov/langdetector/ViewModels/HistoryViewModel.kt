package ru.pudovikov.langdetector.ViewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.realm.Realm
import io.realm.RealmConfiguration
import ru.pudovikov.langdetector.AuxiliaryClasses.HttpRequestController
import ru.pudovikov.langdetector.AuxiliaryClasses.RealmController
import ru.pudovikov.langdetector.Models.HistoryModel

class HistoryViewModel(application: Application) : AndroidViewModel(application) {

    private var realmController = RealmController(getApplication())
    private var compositeDisposable = CompositeDisposable()

    fun getAllHistory(): Single<ArrayList<HistoryModel>> {
        return Single.create { observer ->
            compositeDisposable.add(
                realmController.retrieveAllHistory().subscribe { historyModels ->
                    observer.onSuccess(historyModels)
            })
        }
    }

    override fun onCleared() {
        realmController.close()
        compositeDisposable.dispose()
        super.onCleared()
    }
}