package ru.pudovikov.langdetector.ViewModels

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import io.realm.RealmConfiguration
import ru.pudovikov.langdetector.AuxiliaryClasses.ConvertingController
import ru.pudovikov.langdetector.AuxiliaryClasses.HttpRequestController
import ru.pudovikov.langdetector.AuxiliaryClasses.RealmController
import ru.pudovikov.langdetector.Models.HistoryModel
import ru.pudovikov.langdetector.R
import java.util.*
import java.util.concurrent.TimeUnit

class NewTextFragmentViewModel(application: Application) : AndroidViewModel(application) {

    private var httpRequestController = HttpRequestController()
    private var realmController = RealmController(getApplication())

    fun detectLanguage(inputText: String): Single<String> {
        return Single.create {observer ->
            httpRequestController.makeRequest(inputText)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ response ->
                        val stringResult = ConvertingController.convertStreamToString(response)
                        val jsonResult = ConvertingController.convertStringJson(stringResult)
                        val firstLanguageItem =(jsonResult.get("languages").asJsonArray).get(0)
                        val language = firstLanguageItem.asJsonObject.get("language").asString

                        val historyModel = HistoryModel()
                        historyModel.sourceText = inputText
                        historyModel.language = language.toUpperCase()
                        realmController.saveHistory(historyModel)

                        observer.onSuccess(language.toUpperCase())
                    }, { error ->
                        observer.onError(error)
                    })
        }
    }

    override fun onCleared() {
        realmController.close();
        super.onCleared()
    }
}