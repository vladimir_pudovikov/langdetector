package ru.pudovikov.langdetector.Activities

import android.content.pm.PackageManager
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main_content.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentController
import androidx.fragment.app.FragmentTransaction
import com.jakewharton.rxbinding2.view.clicks
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposables
import kotlinx.android.synthetic.main.fragment_new_text.*
import ru.pudovikov.langdetector.AuxiliaryClasses.DialogController
import ru.pudovikov.langdetector.Fragments.HistoryFragment
import ru.pudovikov.langdetector.Fragments.NewTextFragment
import ru.pudovikov.langdetector.R


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        /*
        * provides a handy way to tie together
        * the functionality of DrawerLayout and ActionBar
        * */
        val drawerToggle = ActionBarDrawerToggle(
                this, drawerLayout, toolbar,
                R.string.drawerOpen, R.string.drawerСlose)
        drawerLayout.addDrawerListener(drawerToggle)
        drawerToggle.syncState()

        replaceFragmentInActivity(NewTextFragment())

        navigationView.setNavigationItemSelectedListener(this)
        navigationView.getMenu().getItem(0).setChecked(true);
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // handle navigation view item clicks
        when (item.itemId) {
            R.id.newTextDrawerItem -> {
                replaceFragmentInActivity(NewTextFragment())
            }
            R.id.historyDrawerItem -> {
                replaceFragmentInActivity(HistoryFragment())
            }
        }

        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    fun replaceFragmentInActivity(fragment: Fragment) {
        val fragmentManager = getSupportFragmentManager()
        val transaction = fragmentManager.beginTransaction()
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.fragmentContainerLayout, fragment)
        transaction.commit()
    }
}
