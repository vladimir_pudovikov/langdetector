package ru.pudovikov.langdetector.AuxiliaryClasses

import android.content.Context
import android.os.IBinder
import android.view.inputmethod.InputMethodManager

object KeyboardController {
    fun hideKeyboard(context: Context, windowToken: IBinder?) {
        val inputManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(windowToken, 0)
    }
}