package ru.pudovikov.langdetector.AuxiliaryClasses

import io.reactivex.Single
import okhttp3.*
import org.w3c.dom.Document
import org.w3c.dom.Element
import java.io.ByteArrayInputStream
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.net.URLEncoder
import java.util.HashMap
import java.util.concurrent.TimeUnit
import javax.xml.parsers.DocumentBuilderFactory
import okhttp3.Challenge
import android.net.wifi.hotspot2.pps.Credential
import io.reactivex.SingleEmitter
import org.json.JSONObject
import ru.pudovikov.langdetector.Constants.HttpErrorConstants
import ru.pudovikov.langdetector.Credentials.WatsonCredentials
import android.R.string
import android.icu.util.UniversalTimeScale
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.stream.JsonReader
import okhttp3.FormBody
import okhttp3.RequestBody
import java.net.Proxy
import okhttp3.HttpUrl


class HttpRequestController {

    // OkHttpClient instance
    private val okHttpClient: OkHttpClient

    //constructor
    init {
        okHttpClient = OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build()
    }

    fun makeRequest(sourceText: String): Single<Response> {
        return Single.create { observer ->
            okHttpClient.newCall(buildHttpRequest(sourceText)).enqueue(onResponseHandler(observer))
        }
    }

    private fun onResponseHandler(observer: SingleEmitter<Response>): Callback {
        return object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                observer.onError(e)
                return
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                if (!response.isSuccessful) {
                    observer.onError(Exception(HttpErrorConstants.NO_SUCCESS))
                    return
                }
                observer.onSuccess(response)  // pass the data to observer
            }
        }
    }

    private fun buildHttpRequest(sourceText: String): Request {
        val dataToSend = RequestBody.create(MediaType.parse("text/plain"), sourceText)
        val apiKey = Credentials.basic(WatsonCredentials.USERNAME, WatsonCredentials.USERPASSWORD)

        return Request.Builder()
                .url(WatsonCredentials.URL + WatsonCredentials.URL_APPENDIX)
                .addHeader("Authorization", apiKey)
                .post(dataToSend)
                .build()
    }
}
