package ru.pudovikov.langdetector.AuxiliaryClasses

import okhttp3.Response
import com.google.gson.JsonObject
import com.google.gson.JsonElement
import com.google.gson.Gson


object ConvertingController {
    fun convertStreamToString(response: Response): String {
        val reader = response.body()!!.charStream()
        val stringResult = StringBuilder()

        for (line in reader.readLines()) {
            stringResult.append (line)
        }

        return stringResult.toString()
    }

    fun convertStringJson(jsonString: String): JsonObject {
        val gson = Gson()
        val element = gson.fromJson(jsonString, JsonElement::class.java)
        return element.getAsJsonObject()
    }
}