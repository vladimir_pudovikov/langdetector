package ru.pudovikov.langdetector.AuxiliaryClasses

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import androidx.fragment.app.FragmentActivity
import io.reactivex.Completable
import io.reactivex.Single
import ru.pudovikov.langdetector.R

class DialogController(private var context: Context?) {

    private var alertDialog: AlertDialog.Builder

    init {
        alertDialog = AlertDialog.Builder(context)
        alertDialog.setCancelable(false)
    }

    fun showAlertDialog(title: String, message: String): Completable {
        return Completable.create { observer ->
            alertDialog.setTitle(title)
            alertDialog.setMessage(message)
            alertDialog.setNeutralButton(context?.getString(R.string.close)) { dialog, whichButton ->
                dialog.cancel()
                observer.onComplete()
            }

            //display a dialog
            alertDialog.create().show()
        }
    }
}
