package ru.pudovikov.langdetector.AuxiliaryClasses

import android.content.Context
import okhttp3.Response
import com.google.gson.JsonObject
import com.google.gson.JsonElement
import com.google.gson.Gson
import io.reactivex.Single
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmResults
import ru.pudovikov.langdetector.Models.HistoryModel
import java.util.*
import kotlin.collections.ArrayList


class RealmController(context: Context) {
    private var realm: Realm

    init {
        Realm.init(context);
        Realm.setDefaultConfiguration(RealmConfiguration.Builder().build());
        realm = Realm.getDefaultInstance()
    }

    fun saveHistory(historyModel: HistoryModel) {
        realm.executeTransactionAsync{ realm ->
            val newHistoryRecord = realm.createObject(HistoryModel::class.java)
            newHistoryRecord.sourceText = historyModel.sourceText
            newHistoryRecord.language = historyModel.language
        }
    }

    fun retrieveAllHistory(): Single<ArrayList<HistoryModel>> {
        return Single.create { observer ->
            realm.executeTransactionAsync{ realm ->
                val allHistoryRecords = realm.where(HistoryModel::class.java).findAll()
                val allHistoryModels = ArrayList<HistoryModel>()
                for (historyRecord in allHistoryRecords) {
                    allHistoryModels.add(HistoryModel(historyRecord.sourceText, historyRecord.language))
                }
                observer.onSuccess(allHistoryModels)
            }
        }
    }

    fun close() {
        realm.close()
    }
}